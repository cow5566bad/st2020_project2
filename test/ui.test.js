const puppeteer = require('puppeteer');
const url = 'http://localhost:3000'
const openBrowser = true; 
const path = require('path');

// 1 - example
test('[Content] Login page title should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    let title = await page.$eval('body > div > form > div:nth-child(1) > h3', (content) => content.innerHTML);
    expect(title).toBe('Login');
    await browser.close();
})


// 2 
test('[Content] Login page join button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    const selector = "body > div.centered-form__form > form > div:nth-child(4) > button";
    let join = await page.$eval(selector, content => content.innerHTML);
    expect(join).toBe('Join');

    await browser.close();
})

// 3 - example
test('[Screenshot] Login page', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.screenshot({path: path.join(__dirname, 'screenshots/login.png') });
    await browser.close();
})

// 4
test('[Screenshot] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 500}); 
    await page.screenshot({path: path.join(__dirname, 'screenshots/welcome.png') });

    await browser.close();
})

// 5
test('[Screenshot] Error message when you login without user and room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.keyboard.press('Enter', {delay: 200});
    await page.screenshot({path: path.join(__dirname, 'screenshots/error.png') });

    await browser.close();
})

// 6
test('[Content] Welcome message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 200});

    const selector = "#messages > li > div.message__body > p"
    let welcomeMsg = await page.$eval(selector, content => content.innerHTML);
    expect(welcomeMsg).toBe('Hi John, Welcome to the chat app');
    
    await browser.close();
})

// 7 - example
test('[Behavior] Type user name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', 100);
    let userName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(2) > input[type=text]').value;
    });
    expect(userName).toBe('John');

    await browser.close();
})

// 8
test('[Behavior] Type room name', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    const roomName = 'RoomTest';
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', roomName, 100);
    let fetchName = await page.evaluate(() => {
        return document.querySelector('body > div > form > div:nth-child(3) > input[type=text]').value;
    });
    expect(fetchName).toBe(roomName);

    await browser.close();
})

// 9 - example
test('[Behavior] Login', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    await page.waitForSelector('#users > ol > li');    
    let member = await page.evaluate(() => {
        return document.querySelector('#users > ol > li').innerHTML;
    });

    expect(member).toBe('John');
    
    await browser.close();
})


// 10
test('[Behavior] Login 2 users', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});

    await page.waitForSelector('#users > ol > li:nth-child(1)');
    await page.waitForSelector('#users > ol > li:nth-child(2)');
    
    let member = await page.evaluate(() => {
        return document.querySelector("#users > ol > li:nth-child(1)").innerHTML;
    });

    let member2 = await page.evaluate(() => {
        return document.querySelector("#users > ol > li:nth-child(2)").innerHTML;
    });

    expect(member).toBe('John');
    expect(member2).toBe('Mike');

    await browser.close();
    await browser2.close();
})

// 11
test('[Content] The "Send" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    const selector = "#message-form > button";
    let sendButton = await page.$eval(selector, content => content.innerHTML);
    expect(sendButton).toBe('Send');
    await browser.close();
})

// 12
test('[Behavior] Send a message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    await page.waitForSelector("#message-form > input[type=text]");
    await page.type("#message-form > input[type=text]", 'Hello~', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    await page.waitForSelector('#messages > li:nth-last-child(1) > div.message__body > p');
    let sentMsg = await page.$eval("#messages > li:nth-last-child(1) > div.message__body > p", content => content.innerHTML)
    expect(sentMsg).toBe('Hello~');
    await browser.close();
})
 
// 13
test('[Behavior] John says "Hi" and Mike says "Hello"', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);
    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    const browser2 = await puppeteer.launch({ headless: !openBrowser });
    const page2 = await browser2.newPage();
    await page2.goto(url);
    await page2.type('body > div > form > div:nth-child(2) > input[type=text]', 'Mike', {delay: 100});
    await page2.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100}); 
    
    await page.type("#message-form > input[type=text]", 'Hi', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});

    await page2.type("#message-form > input[type=text]", 'Hello', {delay: 100});
    await page2.keyboard.press('Enter', {delay: 100});
    
    let sentMsg = await page.$eval("#messages > li:nth-last-child(2) > div.message__body > p", content => content.innerHTML)
    expect(sentMsg).toBe('Hi');

    let sentMsg2 = await page.$eval("#messages > li:nth-last-child(1) > div.message__body > p", content => content.innerHTML)
    expect(sentMsg2).toBe('Hello');

    await browser.close();
    await browser2.close();
})

// 14
test('[Content] The "Send location" button should exist', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });
    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100}); 

    const selector = "#send-location";
    let sendButton = await page.$eval(selector, content => content.innerHTML);
    expect(sendButton).toBe('Send location');
    await browser.close();
})


// 15
test('[Behavior] Send a location message', async () => {
    const browser = await puppeteer.launch({ headless: !openBrowser });

    const page = await browser.newPage();
    await page.goto(url);

    await page.type('body > div > form > div:nth-child(2) > input[type=text]', 'John', {delay: 100});
    await page.type('body > div > form > div:nth-child(3) > input[type=text]', 'R1', {delay: 100});
    await page.keyboard.press('Enter', {delay: 100});
    
    await page.waitForSelector('#send-location');
    await page.$eval('#send-location', elem => elem.click());

    await browser.close();
})
