var socket = io(); // in order to listen for data from server & send data to the server

function scrollToBottom() {
    // Selectors
    var messages = $('#messages');
    var newMessage = messages.children('li:last-child');
    var clientHeight = messages.prop('clientHeight');
    var scrollTop    = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight();

    if(clientHeight + scrollTop + newMessageHeight + lastMessageHeight >= scrollHeight) {
        messages.scrollTop(scrollHeight);
    }
}

socket.on('connect', function() {
  // console.log('Connected to server');
  var params = $.deparam(window.location.search); // get name and room from URL

  socket.emit('join', params, function (err) {
        if (err) {
            // alert(err);
            Swal.fire({ icon: 'error', title: 'Oops...', text: err, showConfirmButton: false, timer: 1500});
            // window.location.href = '/'; // redirect
            setTimeout(() => {window.location.href = '/';}, 1500);
        } else {
            console.log('Welcome!');
            Swal.fire({ position: 'top-end', icon: 'success', title: 'Welcome!', showConfirmButton: false, timer: 1500 })
        }
  })

});

socket.on('disconnect', function() {
  console.log('Disconnected from server');
})

socket.on('updateUserList', function (users) {
    // console.log('Users List', users);
    var ol = $('<ol></ol>');

    users.forEach(function (user) {
        ol.append($('<li></li>').text(user));
    })

    $('#users').html(ol);
});

socket.on('newMessage', function(msg) { // custom event
    var formattedTime = moment(msg.createdAt).format('h:mm a'); 
    var template = $('#message-template').html();              
    var html = Mustache.render(template, {                    
        text: msg.text,
        from: msg.from,
        createAt: formattedTime
    });

    $('#messages').append(html);
    scrollToBottom();
}); 

socket.on('newLocationMessage', function (msg) {
    console.log(msg);
    var formattedTime = moment(msg.createdAt).format('h:mm a');
    var template = $('#location-message-template').html();
    var html = Mustache.render(template, {
        from:     msg.from,
        createAt: formattedTime,
        url:      msg.url,
        lat:      msg.lat,
        lng:      msg.lng
    })

    $('#messages').append(html);
    scrollToBottom();
});

// send button event handler
$('#message-form').on('submit', function (e) {
    e.preventDefault();

    var messageTextbox = $('[name=message]');

    socket.emit('createMessage', {
        from: 'User',
        text: messageTextbox.val()
    }, function () {
        messageTextbox.val(''); //清空輸入欄
    });
});

var locationButton = $('#send-location');
locationButton.on('click', function() {
    if (!navigator.geolocation) {
        // return alert('Geolocation not supported by your browser.');
        return Swal.fire({ icon: 'error', title: 'Oops...', text: 'Geolocation not supported by your browser.'});
    }

    locationButton.attr('disabled', 'disabled').text('Sending location...'); //disabled按鈕直到getCurrentPosition執行完成
    console.log('onclick works')
    navigator.geolocation.getCurrentPosition(function (position) {
        locationButton.removeAttr('disabled').text('Send location'); // enable button
        socket.emit('createLocationMessage', {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        })
    }, function () {
        locationButton.removeAttr('disabled').text('Send location'); // enable button
        // alert('Unable to fetch location.');
        Swal.fire({ icon: 'error', title: 'Oops...', text: 'Unable to fetch location.'});
    });
});

/*
待新增功能

1. 顯示使用者位置(根據經緯度 顯示: 來自XXX附近)
2. alert改sweet alert
*/